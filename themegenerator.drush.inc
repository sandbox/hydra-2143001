<?php
/**
 * @file Drush themegenerator commands.
 */

/**
 * Implementation of hook_drush_help().
 */
function themegenerator_drush_help($section) {}

/**
 * Implementation of hook_drush_command().
 */
function themegenerator_drush_command() {
  $items['generate-theme'] = array(
    'description' => "if available, it'll run yeoman generator-drupaltheme for your current installation",
    'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_FULL,
    'aliases' => array('gt'),
    'options' => array(),
    'examples' => array(),
  );
  return $items;
}

/**
 * Command callback for `drush generate-theme`
 *
 * Start yeoman generator-drupaltheme with a selected starterkit.
 */
function drush_themegenerator_generate_theme() {

  $options = drush_themegenerator_get_starterkits();
  if(!empty($options)) {
    // Prompt for a base theme.
    if (!$starterkit_path = drush_choice($options, dt('Please choose a starterkit for your new theme'))) {
      return;
    }
    drush_set_option('starterkit_path', $starterkit_path);
    // Let the user choose a destination.
    if (!$destination = drush_themegenerator_destination_choice(dt('Please choose a destination. This is where your sub-theme will be placed'))) {
      return;
    }
    drush_set_option('destination', $destination);
  }
  drush_print_r("yo drupaltheme '" . drush_get_option('starterkit_path') . "' '" . drush_get_option('destination') . "'");
  drush_shell_exec_interactive("yo drupaltheme '" . drush_get_option('starterkit_path') . "' '" . drush_get_option('destination') . "'");
}

/**
 * Helper function that asks for the desired destination of a subtheme.
 *
 * @param $message
 *   The message that should be displayed.
 *
 * @return bool|string
 *   The given destination using the pattern "type:destination"
 *   (e.g. "site:all") or FALSE if the operation was cancelled.
 */
function drush_themegenerator_destination_choice($message) {
  drush_print($message);

  // Let the user choose a destination.
  $options = array(
    'site' => dt("Site (e.g. 'all' or 'example.com')"),
    'profile' => dt('Installation profile'),
    'theme' => dt('Parent theme'),
  );

  if (!$type = drush_choice($options, dt('Please choose a destination type.'))) {
    return FALSE;
  }

  switch ($type) {
    case 'site':
      if (!$destination = drush_choice(drush_themegenerator_sites(), dt('Please choose a site.'))) {
        return FALSE;
      }
      return 'sites/' .  $destination . '/themes';

    case 'profile':
      require_once DRUPAL_ROOT . '/includes/install.core.inc';

      $options = array();
      foreach (install_find_profiles() as $profile) {
        $info = drupal_parse_info_file(dirname($profile->uri) . '/' . $profile->name . '.info');
        $options[$profile->name] = $info['name'];
      }

      if (!$destination = drush_choice($options, dt('Please choose an installation profile.'))) {
        return FALSE;
      }
      return 'profile/' . $destination;

    case 'theme':
      if (!$destination = drush_themegenerator_theme_choice(dt('Please choose a theme.'))) {
        return FALSE;
      }
      return drupal_get_path('theme', $destination);

    default:
      return 'sites/all/themes';
  }
}

/**
 * Helper function for printing a list of available themes.
 *
 * @param $message
 *   The message that should be displayed.
 *
 * @return bool|string
 *   The machine-readable name of the chosen theme or FALSE if the operation was
 *   cancelled.
 */
function drush_themegenerator_theme_choice($message) {
  $options = array();
  foreach (list_themes() as $key => $info) {
    $options[$key] = $info->name;
  }
  return drush_choice($options, $message);
}

/**
 * Retrieve an array of available sites.
 *
 * @return array
 *   An array that contains all the available sites in a multisite environment.
 */
function drush_themegenerator_sites() {
  $sites = array();
  // Look up the available sites by iterating over the contents of the sites
  // directory.
  $files = new DirectoryIterator(DRUPAL_ROOT . '/sites');
  foreach ($files as $file) {
    // The sites/default folder is not a valid destination.
    if ($file->isDir() && !$file->isDot() && $file->getFileName() != 'default') {
      $name = $file->getFileName();
      $sites[$name] = $name;
    }
  }
  return $sites;
}

/**
 * Implementation of drush_themegenerator_get_starterkits().
 *
 * Helperfunction to get a list of all starterkits from various locations.
 * Currently supported locations are:
 *    - ~/.drush_themegenerator/starterkits/*
 *    - In a drupal theme.
 *
 * @return array
 *   A list of all available starterkits systemwide.
 */
function drush_themegenerator_get_starterkits() {
  $kits['default'] = 'Default';

  $starterkits = drush_themegenerator_get_starterkits_from_global_directory();
  $starterkits = array_merge($starterkits, drush_themegenerator_get_starterkits_from_theme());

  foreach ($starterkits as $delta => $basetheme) {
    foreach ($basetheme as $key => $path) {
      $starterkit = drush_themegenerator_get_starterkit_info($path);
      $kits[$path] = $starterkit->source . ' - ' . $starterkit->name;
    }
  }

  return $kits;
}

/**
 * Implementation of drush_themegenerator_get_starterkits_from_theme().
 *
 * @return array
 *   A list of all starterkits, provided by themes in your current drupal
 *   context.
 */
function drush_themegenerator_get_starterkits_from_theme() {
  $path_to_drupal = drush_get_context('DRUSH_DRUPAL_ROOT');

  foreach (drush_themegenerator_list_themes_with_starterkits() as $key => $value) {
    $starterkit_path = drush_themegenerator_get_themes_starterkit_path($key);
    $directories[$key] = glob($path_to_drupal . '/' . drupal_get_path('theme', $key) . '/' . $starterkit_path . '/*' , GLOB_ONLYDIR);
  }

  return $directories;
}

/**
 * Implementation of drush_themegenerator_get_starterkits_from_global_directory().
 *
 * Helperfunction to look up for global starterkits.
 *
 * @return array|bool
 *   Eather a list of all availble global starterkits, or FALSE.
 */
function drush_themegenerator_get_starterkits_from_global_directory() {
  if (isset($_SERVER['HOME'])) {
    $home = $_SERVER['HOME'];
  }
  elseif (isset($_SERVER['HOMEDRIVE'])) {
    // TODO: This shoud be tested on windows.
    $home = $_SERVER['HOMEDRIVE'] . $_SERVER['HOMEPATH'];
  }

  return isset($home) ? array(glob($home . '/.drush_themegenerator/starterkits/*' , GLOB_ONLYDIR)) : FALSE;
}

/**
 * Implementation of drush_themegenerator_get_starterkit_info().
 *
 * @param $starterkit_path
 *   The path of a starterkit.
 *
 * @return object
 *   Json decode object, containing all relevant metadata about a starterkit.
 */
function drush_themegenerator_get_starterkit_info($starterkit_path) {
  $handle = @fopen($starterkit_path . '/package.json', "r");
  $starterkit_info = '';
  if ($handle) {
    while (($buffer = fgets($handle, 4096)) !== false) {
      $starterkit_info .= $buffer;
    }
    if (!feof($handle)) {
      return FALSE;
    }
    fclose($handle);
  }

  return json_decode($starterkit_info);
}

/**
 * Implementation of drush_themegenerator_basetheme_list().
 *
 * Helperfunction to get a list of all themes, supporting yeoman theme
 * generator.
 *
 * @return array
 *   A list of themes, taht are providing starterkits.
 */
function drush_themegenerator_list_themes_with_starterkits() {
  $theme_list = list_themes();
  foreach (list_themes() as $key => $info) {
    if(isset($info->info['themegenerator']['starterkits'])) {
      $options[$key] = $info->info['name'];
    }
  }

  return $options;
}

/**
 * Implementation of drush_themegenerator_get_starterkit_path().
 *
 * @param $theme
 *  A string, containing the name of a theme.
 *
 * @return string
 *   The path where the starterkit is defined in the theme.
 */
function drush_themegenerator_get_themes_starterkit_path($theme) {
  $theme_list = list_themes();
  return $theme_list[$theme]->info['themegenerator']['starterkits'];
}
